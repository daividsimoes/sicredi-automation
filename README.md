# Sicredi Automation

This project is aimed to automate tests that refer Sicredi Application.

# Cucumber best practices
- Write scenarios more declarative and less imperative, in other words don't describe step by step, describe only the objective.
- Begin all sentences with capital letters.
- Don't use period.
- Use reserved words **Given**, **When** and **Then** only once per sentence, if you need more details use **And** or **But**.
- Never do more than one validation on **Then** if you need to do more validations use **And** or **But**.
- When there are similar scenarios where only values changes, try to use Scenario Schemes.
- Try to reuse steps when possible.

# Project Setup
- Java 1.8
- Maven 3.3 or Higher
- Git
- IntelliJ Community

# Configure IntelliJ Community IDE
### Lombok
- Open IntelliJ Community IDE
- Go to "File -> Settings"
- Go to "Plugins"
- Search for "Lombok" and install

### Cucumber For Java
- Open IntelliJ Community IDE
- Go to "File -> Settings"
- Go to "Plugins"
- Search for "Cucumber for Java" and install

### Code Completion
- Open IntelliJ Community IDE
- Go to "File -> Settings"
- Go to "Editor -> General -> Code Completion"
- Uncheck "Match case" option

### Auto Import
- Open IntelliJ Community IDE
- Go to "File -> Settings"
- Go to "Editor -> General -> Auto Import"
- Check "Add unambiguous imports on the fly" and "Optimize imports on the fly" options

# Configure Test Runner
### JUnit
- Open IntelliJ Community IDE
- Go to "Run -> Edit Configuration"
- Go to "Templates -> Junit"
- In the field "VM options" add the following text: -Dspring.profiles.active=dev
- Check "Show this page" option

### Cucumber Java
- Open IntelliJ Community IDE
- Go to "Run -> Edit Configuration"
- Go to "Templates -> Cucumber Java"
- In the field "VM options" add the following text: -Dspring.profiles.active=dev
- Check "Show this page" option

# Clone Project
- Open a terminal
- Add the command: git clone https://gitlab.com/daividsimoes/sicredi-automation.git
- Add username and password

# Running tests by terminal
- Start Sicredi Application following steps in https://github.com/desafios-qa-automacao/desafio-sicredi
- Open a terminal
- Go to project folder
- Execute command: mvn test

# Running tests by IntelliJ
All Runners classes are in the directory: src -> test -> java -> br.com.automation.sicredi.test.runner

- Start Sicredi Application following steps in https://github.com/desafios-qa-automacao/desafio-sicredi
- Right click on Runner class
- Select "Run <'RUNNER_CLASS_NAME'>" option
- Click "Run" button in the view