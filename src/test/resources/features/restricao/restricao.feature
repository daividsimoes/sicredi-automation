Feature: API Restrição

  Scenario Outline: Validar consulta de CPF com restrição
    Given Que seja feita uma consulta na API de restrição utilizando o cpf <CPF>
    Then Status code deve ser 200
    And Deve retornar a mensagem "O CPF {0} tem problema"
    Examples:
    | CPF           |
    | "97093236014" |
    | "60094146012" |
    | "84809766080" |
    | "62648716050" |
    | "26276298085" |
    | "01317496094" |
    | "55856777050" |
    | "19626829001" |
    | "24094592008" |
    | "58063164083" |

  Scenario: Validar consulta de CPF sem restrição
    Given Que possua um cpf sem restrição
    When Fizer a consulta na APi de restrição
    Then Status code deve ser 204
