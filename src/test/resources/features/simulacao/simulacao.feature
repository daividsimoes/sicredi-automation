Feature: API Simulação

  Scenario: Validar inserção de simulação
    Given Que exista uma simulação a ser adicionada
    When Fizer a requisição para esta simulação
    Then Status code deve ser 201 para a simulação adicionada
    And Deve adicionar a simulação com sucesso

  Scenario: Validar inserção duplicada de simulação
    Given Que exista uma simulação
    When Fizer a requisição para esta simulação
    Then Status code deve ser 400 para a simulação adicionada
    And Deve retornar a mensagem de duplicidade "CPF duplicado"

  Scenario: Validar obrigatoriedade de atributo na inserção de simulação
    Given Que exista uma simulação
    When Fizer a requisição para esta simulação sem informar o nome
    Then Status code deve ser 400 para a simulação adicionada
    And Deve retornar a mensagem de erro "Nome não pode ser vazio"

#    VALOR DA PARCELA NÃO ATUALIZA?????
  Scenario: Validar atualização de simulação
    Given Que exista uma simulação
    When Fizer a requisição para atualizar esta simulação
    Then Status code deve ser 200 para a simulação adicionada
    And Deve atualizar a simulação com sucesso

  Scenario: Validar atualização de simulação inexistente
    Given Que faça uma requisição para atualizar uma simulação inexistente
    Then Status code deve ser 404 para a simulação adicionada
    And Deve retornar a mensagem "CPF {0} não encontrado"

  Scenario: Validar consulta lista de simulações
    Given Que exista uma simulação
    When Fizer uma consulta na API de simulações
    Then Status code deve ser 200 para a lista de simulações
    And Deve retornar a simulação adicionada

  Scenario: Validar consulta de simulação por cpf existente
    Given Que exista uma simulação
    When Fizer uma consulta na API de simulações por cpf
    Then Status code deve ser 200 para a consulta por cpf
    And Deve retornar a simulação do cpf buscado

  Scenario: Validar consulta de simulação por cpf inexistente
    Given Que exista uma simulação
    When Fizer uma consulta na API de simulações por cpf inexistente
    Then Status code deve ser 404 para a consulta por cpf
    And Deve retornar a mensagem "CPF {0} não encontrado"

  Scenario: Validar exclusão de simulação
    Given Que exista uma simulação
    When Fizer a exclusão da simulação
    Then Status code deve ser 200 para a exclusão
    And Não deve retornar a simução
