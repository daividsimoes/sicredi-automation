package br.com.automation.sicredi.test.step.restricao;

import br.com.automation.sicredi.model.response.ResponseObject;
import br.com.automation.sicredi.model.response.restricao.RestricaoResponse;
import br.com.automation.sicredi.service.restricao.RestricaoService;
import br.com.automation.sicredi.test.step.AbstractStepDefinition;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.Assert.assertEquals;

public class RestricaoStepDefinition extends AbstractStepDefinition {

    @Autowired
    private RestricaoService restricaoService;

    private ResponseObject<RestricaoResponse> restricaoResponseObject;

    private String cpf;

    @Given("Que seja feita uma consulta na API de restrição utilizando o cpf {string}")
    public void que_seja_feita_uma_consulta_na_API_de_restricao_utilizando_o_cpf(String cpf) {

        this.cpf = cpf;
        restricaoResponseObject = restricaoService.getRestricao(cpf, RestricaoResponse.class);
    }

    @Given("Que possua um cpf sem restrição")
    public void que_possua_um_cpf_sem_restricao() {

        this.cpf = fakerUtils.generateRandomCpf();
    }

    @When("Fizer a consulta na APi de restrição")
    public void fizer_a_consulta_na_api_de_restricao() {

        restricaoResponseObject = restricaoService.getRestricao(cpf, RestricaoResponse.class);
    }

    @Then("Status code deve ser {int}")
    public void status_code_deve_ser(Integer code) {

        assertEquals(code, restricaoResponseObject.getHttpCode());
    }

    @Then("Deve retornar a mensagem {string}")
    public void deve_retornar_a_mensagem(String message) {

        String messageFormat = textUtils.formatText(message, cpf);
        String mensagem = restricaoResponseObject.getData().getMensagem();

        assertEquals(messageFormat, mensagem);
    }
}
