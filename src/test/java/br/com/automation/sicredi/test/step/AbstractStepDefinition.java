package br.com.automation.sicredi.test.step;

import br.com.automation.sicredi.utils.FakerUtils;
import br.com.automation.sicredi.utils.TextUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public abstract class AbstractStepDefinition {

    @Autowired
    protected TextUtils textUtils;

    @Autowired
    protected FakerUtils fakerUtils;
}
