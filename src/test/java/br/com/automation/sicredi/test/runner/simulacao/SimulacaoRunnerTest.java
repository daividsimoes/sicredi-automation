package br.com.automation.sicredi.test.runner.simulacao;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        features = {"classpath:features/simulacao"},
        glue = {"br.com.automation.sicredi.test.step.simulacao"},
        plugin = {"junit:target/simulacao.xml"}
)

public class SimulacaoRunnerTest {

}
