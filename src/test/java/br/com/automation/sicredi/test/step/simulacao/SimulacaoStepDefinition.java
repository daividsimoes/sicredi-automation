package br.com.automation.sicredi.test.step.simulacao;

import br.com.automation.sicredi.factory.SimulacaoFactory;
import br.com.automation.sicredi.model.request.SimulacaoRequest;
import br.com.automation.sicredi.model.response.ResponseList;
import br.com.automation.sicredi.model.response.ResponseObject;
import br.com.automation.sicredi.model.response.simulacao.SimulacaoResponse;
import br.com.automation.sicredi.service.simulacao.SimulacaoService;
import br.com.automation.sicredi.test.step.AbstractStepDefinition;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class SimulacaoStepDefinition extends AbstractStepDefinition {

    @Autowired
    private SimulacaoService simulacaoService;

    @Autowired
    private SimulacaoFactory simulacaoFactory;

    private ResponseList<SimulacaoResponse> simulacaoResponseList;

    private ResponseObject<SimulacaoResponse> simulacaoResponseObject;

    private ResponseObject<Object> deleteSimulacaoResponseObject;

    private SimulacaoRequest simulacaoRequest;

    private SimulacaoRequest updateSimulacaoRequest;

    private String cpf;

    @Before
    public void exclui_simulacoes() {

        simulacaoResponseList = simulacaoService.getSimulacoes(SimulacaoResponse.class);
        simulacaoResponseList.getData().forEach(simulacao -> {
            simulacaoService.deleteSimulacao(simulacao.getId(), Object.class);
        });
    }

    @Given("Que exista uma simulação a ser adicionada")
    public void que_exista_uma_simulacao_a_ser_adicionada() {

        String nome = fakerUtils.generateRandomName();
        String cpf = fakerUtils.generateRandomCpf();
        String email = fakerUtils.generateRandomEmail();
        BigDecimal valor = new BigDecimal(2000);
        int parcelas = fakerUtils.generateRandomIntNumberBetween();
        boolean seguro = true;

        simulacaoRequest = simulacaoFactory.buildSimulacaoRequest(nome, cpf, email, valor, parcelas, seguro);
    }

    @Given("Que exista uma simulação")
    public void que_exista_uma_simulacao() {

        String nome = fakerUtils.generateRandomName();
        String cpf = fakerUtils.generateRandomCpf();
        String email = fakerUtils.generateRandomEmail();
        BigDecimal valor = new BigDecimal("2000");
        int parcelas = fakerUtils.generateRandomIntNumberBetween();
        boolean seguro = true;

        simulacaoRequest = simulacaoFactory.buildSimulacaoRequest(nome, cpf, email, valor, parcelas, seguro);
        simulacaoResponseObject = simulacaoService.postSimulacao(simulacaoRequest, SimulacaoResponse.class);
    }

    @Given("Que faça uma requisição para atualizar uma simulação inexistente")
    public void que_faça_uma_requisicao_para_atualizar_uma_simulacao_inexistente() {

        String nome = fakerUtils.generateRandomName();
        cpf = fakerUtils.generateRandomCpf();
        String email = fakerUtils.generateRandomEmail();
        BigDecimal valor = new BigDecimal("3000");
        int parcelas = fakerUtils.generateRandomIntNumberBetween();
        boolean seguro = false;

        updateSimulacaoRequest = simulacaoFactory.buildSimulacaoRequest(nome, cpf, email, valor, parcelas, seguro);
        simulacaoResponseObject = simulacaoService.putSimulacao(cpf, updateSimulacaoRequest, SimulacaoResponse.class);
    }

    @When("Fizer a requisição para esta simulação")
    public void fizer_a_requisicao_para_esta_simulacao() {

        simulacaoResponseObject = simulacaoService.postSimulacao(simulacaoRequest, SimulacaoResponse.class);
    }

    @When("Fizer a requisição para esta simulação sem informar o nome")
    public void fizer_a_requisicao_para_esta_simulacao_sem_informar_o_nome() {

        simulacaoRequest.setNome(null);
        simulacaoResponseObject = simulacaoService.postSimulacao(simulacaoRequest, SimulacaoResponse.class);
    }

    @When("Fizer uma consulta na API de simulações por cpf inexistente")
    public void fizer_uma_consulta_na_api_de_simulacoes_por_cpf_inexistente() {

        cpf = fakerUtils.generateRandomCpf();
        simulacaoResponseObject = simulacaoService.getSimulacaoPorCpf(cpf, SimulacaoResponse.class);
    }

    @When("Fizer a requisição para atualizar esta simulação")
    public void fizer_a_requisicao_para_atualizar_esta_simulacao() {

        String nome = fakerUtils.generateRandomName();
        String cpf = simulacaoRequest.getCpf();
        String email = fakerUtils.generateRandomEmail();
        BigDecimal valor = new BigDecimal("1356.55");
        int parcelas = fakerUtils.generateRandomIntNumberBetween();
        boolean seguro = false;

        updateSimulacaoRequest = simulacaoFactory.buildSimulacaoRequest(nome, cpf, email, valor, parcelas, seguro);
        simulacaoResponseObject = simulacaoService.putSimulacao(cpf, updateSimulacaoRequest, SimulacaoResponse.class);
    }

    @When("Fizer uma consulta na API de simulações")
    public void fizer_uma_consulta_na_api_de_simulacoes() {

        simulacaoResponseList = simulacaoService.getSimulacoes(SimulacaoResponse.class);
    }

    @When("Fizer uma consulta na API de simulações por cpf")
    public void fizer_uma_consulta_na_api_de_simulacoes_por_cpf() {

        simulacaoResponseObject = simulacaoService.getSimulacaoPorCpf(simulacaoRequest.getCpf(),
                SimulacaoResponse.class);
    }

    @When("Fizer a exclusão da simulação")
    public void fizer_a_exclusao_da_simulacao() {

        deleteSimulacaoResponseObject = simulacaoService.deleteSimulacao(simulacaoResponseObject.getData().getId(),
                Object.class);
    }

    @Then("Status code deve ser {int} para a simulação adicionada")
    public void status_code_deve_ser_para_a_simulacao_adicionada(Integer code) {

        assertEquals(code, simulacaoResponseObject.getHttpCode());
    }

    @Then("Status code deve ser {int} para a lista de simulações")
    public void status_code_deve_ser_para_lista_de_simulacoes(Integer code) {

        assertEquals(code, simulacaoResponseList.getHttpCode());
    }

    @Then("Status code deve ser {int} para a consulta por cpf")
    public void status_code_deve_ser_para_a_consulta_por_cpf(Integer code) {

        assertEquals(code, simulacaoResponseObject.getHttpCode());
    }

    @Then("Status code deve ser {int} para a exclusão")
    public void status_code_deve_ser_para_a_exclusao(Integer code) {

        assertEquals(code, deleteSimulacaoResponseObject.getHttpCode());
    }

    @Then("Deve adicionar a simulação com sucesso")
    public void deve_adicionar_a_simulacao_com_sucesso() {

        SimulacaoResponse simulacaoResponse = simulacaoResponseObject.getData();
        validaSimulacao(simulacaoRequest, simulacaoResponse);
    }

    @Then("Deve atualizar a simulação com sucesso")
    public void deve_atualizar_a_simulacao_com_sucesso() {

        SimulacaoResponse simulacaoResponse = simulacaoResponseObject.getData();
        validaSimulacao(updateSimulacaoRequest, simulacaoResponse);
    }

    @Then("Deve retornar a simulação adicionada")
    public void deve_retornar_a_simulacao_adicionada() {

        SimulacaoResponse simulacaoResponse = simulacaoResponseList.getData()
                .stream()
                .filter(simulacao -> (simulacao.getId() == simulacaoResponseObject.getData().getId()))
                .findFirst()
                .orElse(null);

        assertNotNull(simulacaoResponseList.getData());

        validaSimulacao(simulacaoRequest, simulacaoResponse);
    }

    @Then("Deve retornar a simulação do cpf buscado")
    public void deve_retornar_a_simulacao_do_cpf_buscado() {

        SimulacaoResponse simulacaoResponse = simulacaoResponseObject.getData();
        validaSimulacao(simulacaoRequest, simulacaoResponse);
    }

    @Then("Não deve retornar a simução")
    public void nao_deve_retornar_a_simulacao() {

        cpf = simulacaoResponseObject.getData().getCpf();
        simulacaoResponseObject = simulacaoService.getSimulacaoPorCpf(cpf, SimulacaoResponse.class);

        String messageFormat = textUtils.formatText("CPF {0} não encontrado", cpf);

        assertEquals(404, (int) simulacaoResponseObject.getHttpCode());
        assertEquals(messageFormat, simulacaoResponseObject.getData().getMensagem());
    }

    @Then("Deve retornar a mensagem {string}")
    public void deve_retornar_a_mensagem(String message) {

        String messageFormat = textUtils.formatText(message, cpf);
        String mensagem = simulacaoResponseObject.getData().getMensagem();

        assertEquals(messageFormat, mensagem);
    }

    @Then("Deve retornar a mensagem de duplicidade {string}")
    public void deve_retornar_a_mensagem_de_duplicidade(String message) {

        String mensagem = simulacaoResponseObject.getData().getMensagem();
        assertEquals(message, mensagem);
    }

    @Then("Deve retornar a mensagem de erro {string}")
    public void deve_retornar_a_mensagem_de_erro(String message) {

        String mensagem = simulacaoResponseObject.getData().getErros().getNome();
        assertEquals(message, mensagem);
    }

    private void validaSimulacao(SimulacaoRequest simulacaoRequest, SimulacaoResponse simulacaoResponse) {

        assertEquals(simulacaoRequest.getNome(), simulacaoResponse.getNome());
        assertEquals(simulacaoRequest.getCpf(), simulacaoResponse.getCpf());
        assertEquals(simulacaoRequest.getEmail(), simulacaoResponse.getEmail());
        assertEquals(simulacaoRequest.getParcelas(), simulacaoResponse.getParcelas());
        assertEquals(simulacaoRequest.isSeguro(), simulacaoResponse.isSeguro());
        assertEquals(simulacaoRequest.getValor().setScale(2), simulacaoResponse.getValor().setScale(2));
    }
}
