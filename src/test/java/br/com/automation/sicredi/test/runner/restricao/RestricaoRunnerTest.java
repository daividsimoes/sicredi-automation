package br.com.automation.sicredi.test.runner.restricao;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        features = {"classpath:features/restricao"},
        glue = {"br.com.automation.sicredi.test.step.restricao"},
        plugin = {"junit:target/restricao.xml"}
)

public class RestricaoRunnerTest {

}
