package br.com.automation.sicredi.utils;

import gherkin.deps.com.google.gson.Gson;
import gherkin.deps.com.google.gson.GsonBuilder;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;

@Component
public class JsonUtils {

    public String converttoJson(Object obj) {

        return new GsonBuilder()
                .setPrettyPrinting()
                .serializeNulls()
                .create()
                .toJson(obj);
    }

    public <T> T convertToObject(String json, Class<T> clazz) {

        return new Gson()
                .fromJson(json, clazz);
    }

    public <T> List<T> convertToList(String json, Class<T> clazz) {

        return Arrays.asList(
                new Gson()
                        .fromJson(json, clazz)
        );
    }
}
