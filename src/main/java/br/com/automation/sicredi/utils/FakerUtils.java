package br.com.automation.sicredi.utils;

import com.github.javafaker.Faker;
import org.springframework.stereotype.Component;

import java.util.Locale;

@Component
public class FakerUtils {

    private Faker faker;

    public FakerUtils() {

        faker = new Faker(new Locale("pt_BR"));
    }

    public String generateRandomName() {

        return faker.name().name();
    }

    public String generateRandomEmail() {

        return faker.internet().emailAddress();
    }

    public int generateRandomIntNumberBetween() {

        return faker.number().numberBetween(2, 48);
    }

    // https://receitasdecodigo.com.br/java/classe-java-completa-para-gerar-e-validar-cpf-e-cnpj
    public String generateRandomCpf() {

        int n = 9;
        int n1 = randomiza(n);
        int n2 = randomiza(n);
        int n3 = randomiza(n);
        int n4 = randomiza(n);
        int n5 = randomiza(n);
        int n6 = randomiza(n);
        int n7 = randomiza(n);
        int n8 = randomiza(n);
        int n9 = randomiza(n);
        int d1 = n9 * 2 + n8 * 3 + n7 * 4 + n6 * 5 + n5 * 6 + n4 * 7 + n3 * 8 + n2 * 9 + n1 * 10;

        d1 = 11 - (mod(d1, 11));

        if (d1 >= 10)
            d1 = 0;

        int d2 = d1 * 2 + n9 * 3 + n8 * 4 + n7 * 5 + n6 * 6 + n5 * 7 + n4 * 8 + n3 * 9 + n2 * 10 + n1 * 11;

        d2 = 11 - (mod(d2, 11));

        if (d2 >= 10) {

            d2 = 0;
        }

        return "" + n1 + n2 + n3 + n4 + n5 + n6 + n7 + n8 + n9 + d1 + d2;
    }

    private int randomiza(int n) {

        int ranNum = (int) (Math.random() * n);
        return ranNum;
    }

    private int mod(int dividendo, int divisor) {

        return (int) Math.round(dividendo - (Math.floor(dividendo / divisor) * divisor));
    }
}
