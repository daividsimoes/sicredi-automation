package br.com.automation.sicredi.utils;

import org.springframework.stereotype.Component;

import java.text.MessageFormat;

@Component
public class TextUtils {

    public String formatText(String text, Object... args) {

        return MessageFormat.format(text, args);
    }
}
