package br.com.automation.sicredi.utils;

import br.com.automation.sicredi.model.response.ResponseList;
import br.com.automation.sicredi.model.response.ResponseObject;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.restassured.http.Headers;
import io.restassured.response.Response;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import static io.restassured.RestAssured.given;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@Slf4j
@Component
public class RequestUtils {

    @Autowired
    private UrlUtils urlUtils;

    @Autowired
    private JsonUtils jsonUtils;

    public <T> ResponseObject<T> getObject(Headers headers, final Class<T> clazz, String endpoint,
                                           Object... args) {

        String url = urlUtils.buildUrl(endpoint, args);

        log.info("REQUEST -> Executing GET on {}", url);
        log.info("REQUEST -> Headers: {}", jsonUtils.converttoJson(headers.asList()));

        final Response response = given()
                .contentType(APPLICATION_JSON_VALUE)
                .headers(headers)
                .get(url);

        T data = convertResponseToObject(response, clazz);

        ResponseObject<T> responseObject = new ResponseObject<>();
        responseObject.setData(data);
        responseObject.setHttpCode(response.getStatusCode());

        return responseObject;
    }

    public <T> ResponseList<T> getList(Headers headers, final Class<T> clazz, String endpoint,
                                       Object... args) {

        String url = urlUtils.buildUrl(endpoint, args);

        log.info("REQUEST -> Executing GET on {}", url);
        log.info("REQUEST -> Headers: {}", jsonUtils.converttoJson(headers.asList()));

        final Response response = given()
                .contentType(APPLICATION_JSON_VALUE)
                .headers(headers)
                .get(url);

        log.info("RESPONSE -> ResponseType: {}", clazz);

        List<T> dataList = new ArrayList<>();
        ArrayList<LinkedHashMap> arrayList = converResponseToList(response);

        for (LinkedHashMap map : arrayList) {

            ObjectMapper mapper = new ObjectMapper();
            T pojo = mapper.convertValue(map, clazz);

            dataList.add(pojo);
        }

        ResponseList<T> responseList = new ResponseList<>();
        responseList.setData(dataList);
        responseList.setHttpCode(response.getStatusCode());

        return responseList;
    }

    public <T> ResponseObject<T> post(Headers headers, Object body, final Class<T> clazz, String endpoint,
                                      Object... args) {

        String url = urlUtils.buildUrl(endpoint, args);

        log.info("REQUEST -> Executing POST on {}", url);
        log.info("REQUEST -> Headers: {}", jsonUtils.converttoJson(headers.asList()));
        log.info("REQUEST -> Body: {}", jsonUtils.converttoJson(body));

        final Response response = given()
                .contentType(APPLICATION_JSON_VALUE)
                .headers(headers)
                .body(body)
                .post(url);

        T data = convertResponseToObject(response, clazz);

        ResponseObject<T> responseObject = new ResponseObject<>();
        responseObject.setData(data);
        responseObject.setHttpCode(response.getStatusCode());

        return responseObject;
    }

    public <T> ResponseObject<T> put(Headers headers, Object body, final Class<T> clazz, String endpoint, Object... args) {

        String url = urlUtils.buildUrl(endpoint, args);

        log.info("REQUEST -> Executing PUT on {}", url);
        log.info("REQUEST -> Headers: {}", jsonUtils.converttoJson(headers.asList()));
        log.info("REQUEST -> Body: {}", jsonUtils.converttoJson(body));

        final Response response = given()
                .contentType(APPLICATION_JSON_VALUE)
                .headers(headers)
                .body(body)
                .put(url);

        T data = convertResponseToObject(response, clazz);

        ResponseObject<T> responseObject = new ResponseObject<>();
        responseObject.setData(data);
        responseObject.setHttpCode(response.getStatusCode());

        return responseObject;
    }

    public <T> ResponseObject<T> delete(Headers headers, final Class<T> clazz, String endpoint,
                                        Object... args) {

        String url = urlUtils.buildUrl(endpoint, args);

        log.info("REQUEST -> Executing DELETE on {}", url);
        log.info("REQUEST -> Headers: {}", jsonUtils.converttoJson(headers.asList()));

        final Response response = given()
                .contentType(APPLICATION_JSON_VALUE)
                .headers(headers)
                .delete(url);

        T data = convertResponseToObject(response, clazz);

        ResponseObject<T> responseObject = new ResponseObject<>();
        responseObject.setData(data);
        responseObject.setHttpCode(response.getStatusCode());

        return responseObject;
    }

    private <T> T convertResponseToObject(Response response, Class<T> clazz) {

        T responseConverted = null;

        log.info("RESPONSE -> ResponseType: {}", clazz);
        log.info("RESPONSE -> Status Code: {}", response.getStatusCode());
        log.info("RESPONSE -> Time: {} ms", response.getTime());

        try {

            if (!response.getBody().asString().isEmpty()) {

                responseConverted = jsonUtils.convertToObject(response.getBody().asString(), clazz);
                log.info("RESPONSE -> Body: {}", jsonUtils.converttoJson(responseConverted));
            }

        } catch (Exception e) {

            throw new RuntimeException(
                    MessageFormat.format("Failure to convert response -> {0}\nException Message -> {1}",
                            response.getBody().print(), e.getMessage()));
        }

        return responseConverted;
    }

    private ArrayList converResponseToList(Response response) {

        ArrayList responseConverted = null;

        log.info("RESPONSE -> Status Code: {}", response.getStatusCode());
        log.info("RESPONSE -> Time: {} ms", response.getTime());

        try {

            if (!response.getBody().asString().isEmpty()) {

                responseConverted = response.getBody().as(ArrayList.class);
                log.info("RESPONSE -> Body: {}", jsonUtils.converttoJson(responseConverted));
            }

        } catch (Exception e) {

            throw new RuntimeException(
                    MessageFormat.format("Failure to convert response -> {0}\nException Message -> {1}",
                            response.getBody().print(), e.getMessage()));
        }

        return responseConverted;
    }
}
