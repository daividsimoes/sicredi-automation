package br.com.automation.sicredi.utils;

import io.restassured.http.Header;
import io.restassured.http.Headers;
import org.springframework.stereotype.Component;

@Component
public class HeaderUtils {

    public Headers getHeader(Header... headers) {

        return new Headers(headers);
    }
}
