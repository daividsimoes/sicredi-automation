package br.com.automation.sicredi.utils;

import br.com.automation.sicredi.properties.PropertiesTst;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.MessageFormat;

@Component
public class UrlUtils {

    @Autowired
    private PropertiesTst propertiesTst;

    public String buildUrl(String endpoint, Object... args) {

        return propertiesTst.getHost().concat(MessageFormat.format(endpoint, args));
    }
}
