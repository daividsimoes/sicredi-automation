package br.com.automation.sicredi.factory;

import br.com.automation.sicredi.model.request.SimulacaoRequest;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;

@Component
public class SimulacaoFactory {

    public SimulacaoRequest buildSimulacaoRequest(String nome, String cpf, String email, BigDecimal valor,
                                                  int parcelas, boolean seguro) {

        return SimulacaoRequest.builder()
                .nome(nome)
                .cpf(cpf)
                .email(email)
                .valor(valor)
                .parcelas(parcelas)
                .seguro(seguro)
                .build();
    }
}
