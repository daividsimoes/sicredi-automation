package br.com.automation.sicredi.properties;

import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
@Getter
@Setter
public class PropertiesTst {

    @Value("${application.sicredi.host}")
    private String host;
}
