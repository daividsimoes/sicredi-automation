package br.com.automation.sicredi.service.restricao;

import br.com.automation.sicredi.model.response.ResponseObject;
import br.com.automation.sicredi.service.AbstractService;
import org.springframework.stereotype.Service;

@Service
public class RestricaoService extends AbstractService {

    private final String URL_GET_RESTRICAO = "/api/v1/restricoes/{0}";

    public <T> ResponseObject<T> getRestricao(String cpf, Class<T> clazz) {

        return requestUtils.getObject(headerUtils.getHeader(), clazz, URL_GET_RESTRICAO, cpf);
    }
}
