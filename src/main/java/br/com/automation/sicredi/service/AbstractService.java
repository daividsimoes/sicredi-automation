package br.com.automation.sicredi.service;

import br.com.automation.sicredi.utils.HeaderUtils;
import br.com.automation.sicredi.utils.RequestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public abstract class AbstractService {

    @Autowired
    protected RequestUtils requestUtils;

    @Autowired
    protected HeaderUtils headerUtils;
}
