package br.com.automation.sicredi.service.simulacao;

import br.com.automation.sicredi.model.response.ResponseList;
import br.com.automation.sicredi.model.response.ResponseObject;
import br.com.automation.sicredi.service.AbstractService;
import org.springframework.stereotype.Service;

@Service
public class SimulacaoService extends AbstractService {

    private final String URL_SIMULACOES = "/api/v1/simulacoes";

    private final String URL_SIMULACAO_POR_CPF_OU_ID = "/api/v1/simulacoes/{0}";

    public <T> ResponseList<T> getSimulacoes(Class<T> clazz) {

        return requestUtils.getList(headerUtils.getHeader(), clazz, URL_SIMULACOES);
    }

    public <T> ResponseObject<T> getSimulacaoPorCpf(String cpf, Class<T> clazz) {

        return requestUtils.getObject(headerUtils.getHeader(), clazz, URL_SIMULACAO_POR_CPF_OU_ID, cpf);
    }

    public <T> ResponseObject<T> postSimulacao(Object body, Class<T> clazz) {

        return requestUtils.post(headerUtils.getHeader(), body, clazz, URL_SIMULACOES);
    }

    public <T> ResponseObject<T> putSimulacao(String cpf, Object body, Class<T> clazz) {

        return requestUtils.put(headerUtils.getHeader(), body, clazz, URL_SIMULACAO_POR_CPF_OU_ID, cpf);
    }

    public <T> ResponseObject<T> deleteSimulacao(int id, Class<T> clazz) {

        return requestUtils.delete(headerUtils.getHeader(), clazz, URL_SIMULACAO_POR_CPF_OU_ID, id);
    }
}
