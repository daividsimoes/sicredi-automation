package br.com.automation.sicredi.model.request;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.math.BigDecimal;

@Getter
@Setter
@Builder
public class SimulacaoRequest implements Serializable {

    private String nome;

    private String cpf;

    private String email;

    private BigDecimal valor;

    private int parcelas;

    private boolean seguro;
}
