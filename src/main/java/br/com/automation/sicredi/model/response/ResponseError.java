package br.com.automation.sicredi.model.response;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class ResponseError implements Serializable {

    private String nome;

    private String cpf;

    private String email;

    private String valor;

    private String parcelas;

    private String seguro;
}
