package br.com.automation.sicredi.model.response;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Response {

    private Integer httpCode;
}
