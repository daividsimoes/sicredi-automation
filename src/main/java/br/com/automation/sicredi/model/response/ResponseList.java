package br.com.automation.sicredi.model.response;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class ResponseList<T> extends Response {

    private List<T> data;
}
