package br.com.automation.sicredi.model.response.restricao;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class RestricaoResponse implements Serializable {

    private String mensagem;
}
