package br.com.automation.sicredi.model.response;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ResponseObject<T> extends Response {

    private T data;
}
