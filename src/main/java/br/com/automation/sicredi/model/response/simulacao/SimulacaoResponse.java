package br.com.automation.sicredi.model.response.simulacao;

import br.com.automation.sicredi.model.response.ResponseError;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.math.BigDecimal;

@Getter
@Setter
public class SimulacaoResponse implements Serializable {

    private int id;

    private String nome;

    private String cpf;

    private String email;

    private BigDecimal valor;

    private int parcelas;

    private boolean seguro;

    private String mensagem;

    private ResponseError erros;
}
